# IPFS Lite
IPFS Lite node with modern UI to support standard use cases of IPFS


## General 
**IPFS Lite** supports the standard uses cases of IPFS (like add, cat, get, publish, etc).
It provides a modern UI to realize such use cases.
The basic characteristics of the app are decentralized, respect of personal data,
open source, free of charge, transparent, free of advertising and legally impeccable.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/threads.server/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=threads.server)

## Documentation

**IPFS Lite** is a decentralized file-sharing application which based on
the following core technologies.
- IPFS (https://ipfs.io/) 
<br>The IPFS technology is used to support the decentralized file sharing use-cases of this application.
    


### **IPFS Lite** versus **IPFS**
This section describes the differences between an **IPFS Lite** node and a regular **IPFS** node.
<br>In general an **IPFS Lite** has the same functionality like an regular node.
There are some small differences which are described here. The reasons are outlined in brackets.
- **No** Gateway Support
<br>An IPFS gateway is not supported [Performance,Security,Android 10]
- **No** CLI and HTTP API Support
<br>No public API is supported, the application itself based on the internal IPFS Core API [Android 10]
- **No** WebUI Support
<br>The WebUI feature is not supported [Performance,Security,Android 10]


