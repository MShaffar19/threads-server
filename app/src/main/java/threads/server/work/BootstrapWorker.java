package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import threads.LogUtils;
import threads.server.services.LiteService;
import threads.server.utils.Network;

public class BootstrapWorker extends Worker {

    private static final String TAG = BootstrapWorker.class.getSimpleName();
    private static final int MIN_PEERS = 10;

    @SuppressWarnings("WeakerAccess")
    public BootstrapWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    public static OneTimeWorkRequest getWork() {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        return new OneTimeWorkRequest.Builder(BootstrapWorker.class)
                .addTag(TAG)
                .setConstraints(builder.build())
                .build();

    }

    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ...");

        try {
            if (Network.isOnline(getApplicationContext())) {
                LiteService.bootstrap(getApplicationContext(), MIN_PEERS);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }
}

