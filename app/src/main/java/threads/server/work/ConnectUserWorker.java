package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;

import threads.LogUtils;
import threads.server.InitApplication;
import threads.server.core.peers.Content;
import threads.server.core.peers.PEERS;
import threads.server.core.peers.User;
import threads.server.ipfs.IPFS;
import threads.server.ipfs.PID;
import threads.server.ipfs.Peer;
import threads.server.ipfs.PeerInfo;

public class ConnectUserWorker extends Worker {

    private static final String TAG = ConnectUserWorker.class.getSimpleName();
    private final PEERS peers;
    private final IPFS ipfs;

    @SuppressWarnings("WeakerAccess")
    public ConnectUserWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
        peers = PEERS.getInstance(getApplicationContext());
        ipfs = IPFS.getInstance(getApplicationContext());
    }

    public static OneTimeWorkRequest getWork(@NonNull String pid) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(Content.PID, pid);

        return new OneTimeWorkRequest.Builder(ConnectUserWorker.class)
                .setInputData(data.build())
                .addTag(TAG)
                .setConstraints(builder.build())
                .build();

    }

    @NonNull
    @Override
    public Result doWork() {

        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start connect [" + pid + "]...");


        try {
            int timeout = InitApplication.getConnectionTimeout(getApplicationContext());


            connect(PID.create(pid));

            peers.resetUserDialing(pid);


            if (!isStopped()) {
                PeerInfo pInfo = ipfs.pidInfo(PID.create(pid));
                if (pInfo != null) {

                    if (peers.getUserPublicKey(pid) == null) {
                        String pKey = pInfo.getPublicKey();
                        if (pKey != null) {
                            if (!pKey.isEmpty()) {
                                peers.setUserPublicKey(pid, pKey);
                            }
                        }
                    }
                    if (!peers.getUserIsLite(pid)) {

                        String agent = pInfo.getAgentVersion();
                        if (!agent.isEmpty()) {
                            peers.setUserAgent(pid, agent);
                            if (agent.endsWith("lite")) {
                                peers.setUserLite(pid);
                            }
                        }
                    }
                }
            }

            if (!isStopped()) {
                PeerInfo pInfo = ipfs.id(PID.create(pid), timeout);
                if (pInfo != null) {

                    if (peers.getUserPublicKey(pid) == null) {
                        String pKey = pInfo.getPublicKey();
                        if (pKey != null) {
                            if (!pKey.isEmpty()) {
                                peers.setUserPublicKey(pid, pKey);
                            }
                        }
                    }
                    if (!peers.getUserIsLite(pid)) {

                        String agent = pInfo.getAgentVersion();
                        if (!agent.isEmpty()) {
                            peers.setUserAgent(pid, agent);
                            if (agent.endsWith("lite")) {
                                peers.setUserLite(pid);
                            }
                        }
                    }
                }
            }

            if (!isStopped()) {
                Peer peerInfo = ipfs.swarmPeer(PID.create(pid));
                String multiAddress = "";
                if (peerInfo != null) {
                    multiAddress = peerInfo.getMultiAddress();
                }

                if (!multiAddress.isEmpty() && !multiAddress.contains(Content.CIRCUIT)) {
                    peers.setUserAddress(pid, multiAddress);
                }
            }


            ipfs.isConnected(PID.create(pid));

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            peers.resetUserWork(pid);
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }


    private void connect(@NonNull PID pid) {

        int timeout = InitApplication.getConnectionTimeout(getApplicationContext());

        if (!ipfs.isConnected(pid)) {
            if (!isStopped()) {
                // now check old addresses
                PEERS peers = PEERS.getInstance(getApplicationContext());
                User user = peers.getUserByPID(pid);
                Objects.requireNonNull(user);
                String address = user.getAddress();
                if (!address.isEmpty() && !address.contains("p2p-circuit")) {
                    String multiAddress = address.concat("/p2p/" + pid.getPid());

                    if (ipfs.swarmConnect(multiAddress, timeout)) {
                        return;
                    }
                }
            }

            if (!isStopped()) {
                ipfs.swarmConnect(pid, timeout);
            }
        }
    }
}

