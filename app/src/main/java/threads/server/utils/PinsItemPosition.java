package threads.server.utils;

@SuppressWarnings("WeakerAccess")
public interface PinsItemPosition {
    int getPosition(long idx);
}
