package threads.server.ipfs;

public enum Reachable {
    UNKNOWN, PUBLIC, PRIVATE
}
